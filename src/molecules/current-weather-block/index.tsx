import React, { useContext } from "react";
import WeatherIcon from "../../atoms/weather-icon-bind-from-openweathermap";
import WeatherBusiness from "../../business/weather-business";
import { Context } from "../../context/app-context";
import numberExtension from "../../utils/number-extension";
import stringExtension from "../../utils/string-extension";
import "./index.scss";

interface iProps {
  cityName: string;
}

class Model {
  icon: string;
  statusText: string;
  humidity: string;
  temperature: number;
  windSpeed: number;

  constructor() {
    this.icon = "";
    this.humidity = "";
    this.statusText = "";
    this.temperature = 0;
    this.windSpeed = 0;
  }
}

const CurrentWeatherBlock = (props: iProps) => {
  const [model, setModel] = React.useState<Model | null>(new Model());

  const context = useContext(Context);

  const getCurrentWeather = async () => {
    const resp = await WeatherBusiness.getCurrentWeatherForecastByCityName(
      props.cityName ? props.cityName : "ho chi minh"
    );

    if (resp) {
      const model = new Model();

      if (resp.main) {
        model.humidity = resp.main.humidity;
        model.temperature = Math.floor(resp.main.temp);
        model.windSpeed = numberExtension.convertMetersPerSecToMPH(
          resp.wind?.speed
        );
      }

      if (resp.weather && resp.weather.length > 0) {
        model.statusText = resp.weather[0].description;
        model.icon = `${resp.weather[0].icon}`;
      }

      setModel(model);
    }
  };

  React.useEffect(() => {
    getCurrentWeather();
  }, [context?.selectedCity]);

  return (
    <div className="current-weather-block">
      <div className="current-weather-block__col">
        <div className="current-weather-block__icon">
          <WeatherIcon icon={model?.icon}></WeatherIcon>
        </div>
        <div className="current-weather-block__text">
          {stringExtension.upperCaseIndex(model?.statusText, 0, 1)}
        </div>
        <div className="current-weather-block__temperature temperature">
          {model?.temperature}
        </div>
      </div>
      <div className="current-weather-block__col align-right">
        <div className="current-weather-block__property">
          <div className="current-weather-block__property__title">
            <i className="wi wi-humidity"></i> Độ ẩm
          </div>
          <div className="current-weather-block__property__content">
            {model?.humidity}%
          </div>
        </div>
        <div className="current-weather-block__property">
          <div className="current-weather-block__property__title">
            <i className="wi wi-strong-wind"></i> Tốc độ gió
          </div>
          <div className="current-weather-block__property__content">
            {model?.windSpeed} mp/h
          </div>
        </div>
      </div>
    </div>
  );
};

export default CurrentWeatherBlock;
