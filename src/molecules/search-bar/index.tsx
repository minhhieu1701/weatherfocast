import React from "react";
import "./index.scss";
import ProvinceBusiness, { TYPE } from "../../business/province-business";
import Province from "../../business/models/province";
import WeatherBusiness from "../../business/weather-business";
import { Action, Context } from "../../context/app-context";
import SelectSearch from "react-select-search";
import stringExtension from "../../utils/string-extension";

//https://react-select-search.com/

interface IOption {
  name: string;
  value: string;
  type?: string;
  items?: IOption[];
}

const SearchBar = () => {
  const [provinces, setProvinces] = React.useState<Array<IOption>>([]);
  const [selectedProvince, setSelectedProvince] = React.useState<string>();

  const context = React.useContext(Context);

  React.useEffect(() => {
    const provinces1 = ProvinceBusiness.getProvinceList({
      type: TYPE.GET_PROVINCE,
    });

    if (provinces1 && provinces1.length > 0) {
      const options: IOption[] = [];

      let iCounter = 0;

      provinces1.forEach((x: Province) => {
        options.push({
          name: x.name,
          value: stringExtension.convertViToEn(x.name),
        });

        iCounter++;
      });

      if (iCounter > 0) {
        setProvinces(options);
        context?.dispatch({
          type: Action.UPDATE_SELECTED_CITY_NAME,
          payload: options[0].value
        });
      }
    }
  }, []);

  React.useEffect(() => {
    if (provinces && provinces.length > 0) {
      WeatherBusiness.getCurrentWeatherForecastByCityName(provinces[0].name);
    }
  }, [provinces]);

  React.useEffect(() => {
    if (selectedProvince) {
      WeatherBusiness.getCurrentWeatherForecastByCityName(selectedProvince);
    }
  }, [provinces, selectedProvince]);

  const onChange = (value: any) => {
    setSelectedProvince(value);

    context?.dispatch({
      type: Action.UPDATE_SELECTED_CITY_NAME,
      payload: value,
    });
  };

  return (
    <div className="search-bar">
      {/* <select
        name="provinces"
        id="provinces"
        placeholder="Chọn thành phố"
        onChange={(event) => onChange(event)}
      >
        {provinces.map((x) => (
          <option key={x.code} value={x.name}>
            {x.name}
          </option>
        ))}
      </select> */}
      <SelectSearch
        options={provinces}
        onChange={onChange}
        value={context?.selectedCity}
        search
        placeholder="Choose your language"
      />
      <div className="search-bar__append">
        <button>
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Search_Icon.svg/1024px-Search_Icon.svg.png"
            alt=""
          />
        </button>
      </div>
    </div>
  );
};

export default SearchBar;
