import React from "react";
import WeatherIcon from "../../atoms/weather-icon-bind-from-openweathermap";
import { IForecastListItem } from "../../context/app-context";
import "./index.scss";

const DateWeatherBlock: React.FC<IForecastListItem> = ({
  icon,
  statusText,
  temperature,
  text,
}: IForecastListItem) => {

  text = text ?? "";

  let text1 = "";
  const arr = text.split("/");
  
  if(arr && arr.length > 0){
    arr.pop();

    const arr1 : string[] = [];

    arr.forEach(x => {
      if(+x < 10){
        arr1.push(`0${x}`);
      }
      else{
        arr1.push(x);
      }
    });

    if(arr1.length > 0){
      text1 = arr1.join("/");
    }
  }
  
  return (
    <div className="date-weather-block" title={`${text} : ${statusText}`}>
      <div className="">{text1}</div>
      <div className="date-weather-block__icon">
        <WeatherIcon icon={icon}></WeatherIcon>
      </div>
      <div className="date-weather-block__temperature temperature">
        {temperature}
      </div>
    </div>
  );
};

export default DateWeatherBlock;
