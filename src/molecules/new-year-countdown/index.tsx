import React, { CSSProperties, useEffect, useState } from "react";
import CountDownBlock from "../../atoms/countdown-block";
import CountdownTitle from "../../atoms/countdown-title";
import dateExtension, { IRemainingTime } from "../../utils/date-extension";

interface Props {}

const NewYearCountDown = (props: Props) => {
  const [timeLeft, setTimeLeft] = useState<IRemainingTime>({
    seconds: 0,
    minutes: 0,
    hours: 0,
    days: 0,
  });

  useEffect(() => {
    if (process.env.REACT_APP_SHOW_COUNTDOWN !== "1") {
      return;
    }

    const timeInterval = setInterval(() => {
      const today = new Date();
      let deadline: Date = new Date();

      if(process.env.REACT_APP_COUNTDOWN_TIME_POINT){
        deadline = new Date(+process.env.REACT_APP_COUNTDOWN_TIME_POINT);
      }

      setTimeLeft(dateExtension.getRemainingTime(today.toString(), deadline.toLocaleDateString()));

      if (
        timeLeft.days === 0 &&
        timeLeft.hours === 0 &&
        timeLeft.minutes === 0 &&
        timeLeft.seconds === 0
      ) {
        clearInterval(timeInterval);
      }
    });
  }, [timeLeft]);

  if (process.env.REACT_APP_SHOW_COUNTDOWN !== "1") {
    return null;
  }

  const customStyle: CSSProperties = {
    paddingTop: "25px",
  };

  return (
    <div className="new-year-countdown">
      <CountdownTitle
        text={process.env.REACT_APP_COUNTDOWN_TITLE}
      ></CountdownTitle>
      <CountDownBlock number={timeLeft.days} title="Ngày"></CountDownBlock>
      <CountDownBlock number={timeLeft.hours} title="Giờ"></CountDownBlock>
      <CountDownBlock number={timeLeft.minutes} title="Phút"></CountDownBlock>
      <CountDownBlock number={timeLeft.seconds} title="Giây"></CountDownBlock>
      <CountdownTitle
        text={process.env.REACT_APP_COUNTDOWN_SUBTITLE}
        customStyle={customStyle}
      ></CountdownTitle>
    </div>
  );
};

export default NewYearCountDown;
