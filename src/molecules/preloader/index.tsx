import * as React from "react";
import PreloaderIconCenter from "../../atoms/preloader-icon-center";
import "./index.scss";

export interface IAppProps {
  isActive: boolean;
}

export default function Preloader(props: IAppProps) {
  return (
    <div className={`preloader ${props.isActive ? "m-fadeIn" : ""}`}>
      <PreloaderIconCenter></PreloaderIconCenter>
    </div>
  );
}
