import provinces from "../data/vietnam-provinces/Index.json";
import Province from "./models/province";

export interface IGetProvinceListProp {
  provinceCode: string | undefined;
  districtCode: string | undefined;
  type: number;
}

const _TYPE = {
  GET_PROVINCE: 1,
  GET_DISTRICT: 2,
  GET_WARD: 3,
};

export class ProvinceBusiness {
  getProvinceList = (props: any) => {
    switch (props.type) { 
      case _TYPE.GET_PROVINCE:
      default:
        if (props.provinceCode) {
        }
        const provinceEntries = Object.entries(provinces);

        const provinceList = new Array<Province>();

        provinceEntries.forEach((key) => {
          const x = new Province();
          x.code = key[1].code;
          x.name = key[0];

          provinceList.push(x);
        });

        return provinceList;
    }
  };
}

export const TYPE = _TYPE;


export default new ProvinceBusiness();