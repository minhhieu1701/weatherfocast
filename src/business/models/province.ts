import Basic from "./province-basic"

export default class Province{
    code!: string
    name!: string
    districts!: Array<Basic>
}