import React from "react";
import { ContextProvider } from "./context/app-context";
import Home from "./pages/home";

const App = () => {
  console.log(process.env)
  return (
    <ContextProvider>
      <Home></Home>
    </ContextProvider>
  );
};

export default App;
