import React from "react";
import { Switch } from "react-router";
import { IRoute } from "./route";
import RouteWithSubRoutes from "./route-with-sub-routes";

interface IProps {
  routes: IRoute[];
}

const Router: React.FC<IProps> = ({routes}) => {
    return (
        <Switch>
            {
                routes.map(route => (
                    <RouteWithSubRoutes key={route.path} {...route}></RouteWithSubRoutes>
                ))
            }
        </Switch>
    );
};