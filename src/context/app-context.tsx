/**
 * Create context
 */

import { Draft } from "immer";
import contextCreator from "./context-creator";

/**
 * Define interface for forecast list item
 */
export interface IForecastListItem {
  text: string;
  statusText: string;
  icon: string;
  temperature: string;
}

/**
 * Define interface for state
 */
export interface IState {
  isShowingPreloader: boolean;
  selectedCity: string;
  forecastList?: Array<IForecastListItem>;
}

/**
 * Define interface for action
 */
interface IAction {
  type: string;
  payload?: any;
}

/**
 * Define state base on interface IState
 */
const initialState: IState = {
  isShowingPreloader: true,
  selectedCity: "",
  forecastList: [],
};

/**
 * Define enum action
 */
export enum Action {
  UPDATE_PRELOADER_STATUS = "UPDATE_PRELOADER_STATUS",
  UPDATE_SELECTED_CITY_NAME = "UPDATE_SELECTED_CITY_NAME",
  UPDATE_FORECAST_LIST = "UPDATE_FORECAST_LIST",
}

/**
 * Define reducer
 */
const reducer = (draft: Draft<IState>, action: IAction) => {
  switch (action.type) {
    case Action.UPDATE_PRELOADER_STATUS:
      draft.isShowingPreloader = action.payload;
      return draft;
    case Action.UPDATE_SELECTED_CITY_NAME:
      draft.selectedCity = action.payload;
      return draft;
    case Action.UPDATE_FORECAST_LIST:
      draft.forecastList = action.payload;
      return draft;
    default:
      return draft;
  }
};

const { Context, ContextProvider } = contextCreator<IState, IAction>(
  initialState,
  reducer
);

export { Context, ContextProvider };
