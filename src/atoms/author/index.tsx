import * as React from 'react';
import "./index.scss";

export interface IAppProps {
    name: string
}

export default function Author (props: IAppProps) {
  return (
    <div className="author">
      {`${props.name} @ ${new Date().getFullYear()}`}
    </div>
  );
}
