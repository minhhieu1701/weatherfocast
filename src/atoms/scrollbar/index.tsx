import * as React from "react";
import "./index.scss";

export interface IAppProps {
  children: React.ReactElement[];
  className: string | "";
}

export default function ScrollContainer(props: IAppProps) {
  return <div className={`scroll ${props.className}`}>{props.children}</div>;
}
