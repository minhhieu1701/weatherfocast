import React from "react";
import "./index.scss";

interface Props {
  number: number;
  title: string;
}

const CountDownBlock = (props: Props) => {
  return (
    <div className="count-down-block">
      <div className="count-down-block__number">
        {props.number < 10 ? `0${props.number}` : props.number}
      </div>
      <div className="count-down-block__title">{props.title}</div>
    </div>
  );
};

export default CountDownBlock;
