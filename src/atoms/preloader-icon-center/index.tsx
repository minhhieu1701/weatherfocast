import * as React from "react";
import "./index.scss";

export interface IAppProps {}

export default function PreloaderIconCenter(props: IAppProps) {
  return <div className="loader"></div>;
}
