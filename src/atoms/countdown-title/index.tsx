import * as React from "react";
import "./index.scss";

export interface IAppProps {
  text?: string;
  customStyle?: React.CSSProperties;
}

export default function CountdownTitle(props: IAppProps) {
  return props.text ? (
    <div
      className="countdown-title"
      style={props.customStyle ? props.customStyle : {}}
    >
      {props.text}
    </div>
  ) : null;
}
