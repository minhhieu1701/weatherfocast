import * as React from "react";
import { useEffect, useState } from "react";

/**
 * icon : this is the icon from open weather map response
 */
export interface IAppProps {
  icon?: string;
}

export default function WeatherIcon(props: IAppProps) {
  const [icon, setIcon] = useState<string>("");

  useEffect(() => {
    switch (props.icon) {
      case "03d":
      default:
        setIcon("wi-cloud");
        break;
      case "04d":
        setIcon("wi-cloudy");
        break;
      case "04n":
        setIcon("wi-cloudy-windy");
        break;
    }
  }, [props.icon]);

  return icon ? <i className={`wi ${icon}`}></i> : null;
}
