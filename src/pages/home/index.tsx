/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import "./index.scss";
import SearchBar from "../../molecules/search-bar";
import DateWeatherBlock from "../../molecules/date-weather-block";
import CurrentWeatherBlock from "../../molecules/current-weather-block";
import { Action, Context, IForecastListItem } from "../../context/app-context";
import weatherBusiness from "../../business/weather-business";
import arrayExtension from "../../utils/array-extension";
import NewYearCountDown from "../../molecules/new-year-countdown";
import ScrollContainer from "../../atoms/scrollbar";
import Preloader from "../../molecules/preloader";
import Author from "../../atoms/author";

const Home: React.FC = (): JSX.Element => {
  const state = React.useContext(Context);

  //#region  methods

  const getForecastByCityName = async function name(cityName: string) {
    if (!state) {
      return;
    }

    const resp = await weatherBusiness.getCurrentForecastByCityName(
      cityName ? cityName : "ho chi minh"
    );

    if (resp) {
      if ("list" in resp) {
        resp.list.forEach((x: any) => {
          x.dateText = new Date(x.dt * 1000).toLocaleDateString("vi");
        });

        const resp1 = arrayExtension.groupBy(resp.list, "dateText");

        if (resp1 && resp1.length > 0) {
          const resp2: IForecastListItem[] = new Array<IForecastListItem>();

          for (let index = 0, length = resp1.length; index < length; index++) {
            const element = resp1[index] as Array<any>;
            if (element && element.length > 0) {
              resp2.push({
                icon: element[1][0].weather[0].icon,
                statusText: element[1][0].weather[0].description,
                temperature: Math.floor(element[1][0].main?.temp).toString(),
                text: element[0],
              });
            }
          }

          state.dispatch({
            type: Action.UPDATE_FORECAST_LIST,
            payload: resp2,
          });
        }
      }
    }
  };

  //#endregion

  React.useEffect(() => {
    if (state) {
      getForecastByCityName(state.selectedCity);
    }
  }, [state?.selectedCity]);

  React.useEffect(() => {
    state?.dispatch({
      type: Action.UPDATE_PRELOADER_STATUS,
      payload: false,
    });
  }, []);

  return (
    <>
      <ScrollContainer className="bg-parallax">
        <div className="home-block">
          <div className="home-block__container">
            <div className="home-block__search-block">
              <div className="home-block__search-block__container">
                <SearchBar></SearchBar>
              </div>
            </div>
            <NewYearCountDown></NewYearCountDown>
            {state && (
              <>
                <div className="home-block__current-weather-block">
                  <div className="home-block__current-weather__container">
                    <CurrentWeatherBlock
                      cityName={state.selectedCity}
                    ></CurrentWeatherBlock>
                  </div>
                </div>
                <>
                  {state.forecastList && (
                    <div className="home-block__list-date">
                      {state.forecastList.map((x, index) => {
                        console.log(index);
                        if (index < 5) {
                          return (
                            <DateWeatherBlock
                              key={x.text}
                              {...x}
                            ></DateWeatherBlock>
                          );
                        }
                      })}
                    </div>
                  )}
                </>
              </>
            )}
          </div>
        </div>
        <Author name="Nguyễn Dương Minh Hiếu"></Author>
      </ScrollContainer>
      <Preloader
        isActive={state ? state.isShowingPreloader : false}
      ></Preloader>
    </>
  );
};

export default Home;
