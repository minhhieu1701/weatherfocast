const arrayExtension = {
  groupBy: function (arr: Array<any>, key: string): Array<any> {
    const temp = arr.reduce(function (rv: any, x: any) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});

    return Object.keys(temp).map((key) => [(key), temp[key]]);
  },
};

export default arrayExtension;
