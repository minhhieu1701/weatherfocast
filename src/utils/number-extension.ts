import csntLanguage from "../data/language";

declare class Language {
  vietnamese: string;
  english: string;
}

const compassPoints: Array<Language> = [
  { vietnamese: "Bắc", english: "North" },
  { vietnamese: "Đông Bắc Bắc", english: "North North East" },
  { vietnamese: "Đông Bắc", english: "North East" },
  { vietnamese: "Đông Đông Bắc", english: "East North East" },
  { vietnamese: "Phía đông", english: "East" },
  { vietnamese: "Đông Đông Nam Bộ", english: "East South East" },
  { vietnamese: "Đông Nam", english: "South East" },
  { vietnamese: "Đông Nam Bộ", english: "South South East" },
  { vietnamese: "Miền Nam", english: "South" },
  { vietnamese: "Tây Nam Bộ", english: "South South West" },
  { vietnamese: "Tây Nam", english: "South West" },
  { vietnamese: "Tây Tây Nam Bộ", english: "West South West" },
  { vietnamese: "Hướng Tây", english: "West" },
  { vietnamese: "Tây Tây Bắc", english: "West North West" },
  { vietnamese: "Tây Bắc", english: "North West" },
  { vietnamese: "Bắc Tây Bắc", english: "North North West" },
];

const numberExtension = {
  convertDegreeToCompassPoint(wind_deg: number, lang: string = "vi"): string {
    const rawPosition = Math.floor(wind_deg / 22.5 + 0.5);
    const arrayPosition = rawPosition % 16;
    switch (lang) {
      case csntLanguage.ENGLISH:
      default:
        return compassPoints[arrayPosition].english;
      case csntLanguage.VIETNAM:
        return compassPoints[arrayPosition].vietnamese;
    }
  },
  convertMetersPerSecToMPH(mps: number): number {
    const mph = +(2.23694 * mps).toFixed(2);
    return mph;
  },
};

export default numberExtension;
