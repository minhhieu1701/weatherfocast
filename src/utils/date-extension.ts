const dateExtension = {
  getRemainingTime: (current: string, target: string) : IRemainingTime => {
    if (!current || !target) {
      // eslint-disable-next-line no-throw-literal
      throw "Đầu vào không hợp lệ";
    }

    try {
      const today = Date.parse(current),
        deadline = Date.parse(target);

      if (today > deadline) {
        throw "Đầu vào không hợp lệ";
      }

      const milliseconds = deadline - today;

      return {
        seconds: Math.floor((milliseconds / 1000) % 60),
        minutes: Math.floor((milliseconds / 1000 / 60) % 60),
        hours: Math.floor((milliseconds / (1000 * 60 * 60)) % 60),
        days: Math.floor((milliseconds / (1000 * 60 * 60 * 24)) % 24),
      };
    } catch {}

    return {
      seconds: 0,
      minutes: 0,
      hours: 0,
      days: 0,
    };
  },
};

export interface IRemainingTime{
  seconds: number,
  minutes: number,
  hours: number,
  days: number
}

export default dateExtension;
