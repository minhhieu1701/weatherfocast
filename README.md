# Weather forecast

#### Config ####
REACT_APP_WEATHER_API_KEY=""
REACT_APP_SHOW_COUNTDOWN=0
REACT_APP_COUNTDOWN_TITLE=""
REACT_APP_COUNTDOWN_SUBTITLE=""
REACT_APP_COUNTDOWN_TIME_POINT=0

**Vietnamese:**
Đây là một dự án nhỏ về trang dự báo thời tiết, sử dụng api của [OpenWeatherMap.org](https://openweathermap.org/)

Cảm ơn


**English:**
This is a small project about weather forecast using api of [OpenWeatherMap.org](https://openweathermap.org/)

Thanks.
